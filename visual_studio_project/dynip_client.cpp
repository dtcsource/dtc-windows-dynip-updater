/****************************************************************************
 * Copyright 2009 Dan Bizdadea (dan.bizdadea@gmail.com), Thomas Goirand
 * 
 ****************************************************************************/

/****************************************************************************
 *	This file is part of Dynamic IP Update Client.
 *
 *  Dynamic IP Update Client is free software: you can redistribute it and/or 
 *	modify it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dynamic IP Update Client is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dynamic IP Update Client.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/


// Win32 Dialog.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define TRAYICONID	1//				ID number for the Notify Icon
#define SWM_TRAYMSG	WM_APP//		the message ID sent to our window


#define SWM_SHOW	WM_APP + 1//	show the window
#define SWM_HIDE	WM_APP + 2//	hide the window
#define SWM_EXIT	WM_APP + 3//	close the window

// Global Variables:
HINSTANCE		hInst;	// current instance
NOTIFYICONDATA	niData;	// notify icon data

// Forward declarations of functions included in this code module:
BOOL				InitInstance(HINSTANCE, int);
BOOL				OnInitDialog(HWND hWnd);
void				ShowContextMenu(HWND hWnd);
ULONGLONG			GetDllVersion(LPCTSTR lpszDllName);

INT_PTR CALLBACK	DlgProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

// This is our saved option memory bloc
t_so saved_options;

// Variables for networking
WORD wVersionRequested;
WSADATA ws;
SOCKET sock;
struct sockaddr_in a;
int dest;
int ii;



int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;
	
	// remove error.log file
	remove(LOG_FILE);

	ZeroMemory(&saved_options,sizeof(saved_options));

	FILE* fp;
	fp = fopen("dtc_dynip_pref.dat","rb");
	if(fp != NULL){
		fread(&saved_options,1,sizeof(saved_options),fp);
		fclose(fp);
	}


	

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) return FALSE;
	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_STEALTHDIALOG);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)||
			!IsDialogMessage(msg.hwnd,&msg) ) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (int) msg.wParam;
}

//	Initialize the window and tray icon
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	// prepare for XP style controls
	InitCommonControls();

	 // store instance handle and create dialog
	hInst = hInstance;
	HWND hWnd = CreateDialog( hInstance, MAKEINTRESOURCE(IDD_DLG_DIALOG),
		NULL, (DLGPROC)DlgProc );
	if (!hWnd) return FALSE;

	// Fill the NOTIFYICONDATA structure and call Shell_NotifyIcon

	// zero the structure - note:	Some Windows funtions require this but
	//								I can't be bothered which ones do and
	//								which ones don't.
	ZeroMemory(&niData,sizeof(NOTIFYICONDATA));

	// get Shell32 version number and set the size of the structure
	//		note:	the MSDN documentation about this is a little
	//				dubious and I'm not at all sure if the method
	//				bellow is correct
	ULONGLONG ullVersion = GetDllVersion(_T("Shell32.dll"));
	if(ullVersion >= MAKEDLLVERULL(5, 0,0,0))
		niData.cbSize = sizeof(NOTIFYICONDATA);
	else niData.cbSize = NOTIFYICONDATA_V2_SIZE;

	// the ID number can be anything you choose
	niData.uID = TRAYICONID;

	// state which structure members are valid
	niData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;

	// load the icon
	niData.hIcon = (HICON)LoadImage(hInstance,MAKEINTRESOURCE(IDI_STEALTHDLG),
		IMAGE_ICON, GetSystemMetrics(SM_CXSMICON),GetSystemMetrics(SM_CYSMICON),
		LR_DEFAULTCOLOR);

	// the window to send messages to and the message to send
	//		note:	the message value should be in the
	//				range of WM_APP through 0xBFFF
	niData.hWnd = hWnd;
    niData.uCallbackMessage = SWM_TRAYMSG;

	// tooltip message
    lstrcpyn(niData.szTip, _T("DTC dynip client v1.0 beta"), sizeof(niData.szTip)/sizeof(TCHAR));

	Shell_NotifyIcon(NIM_ADD,&niData);

	// free icon handle
	if(niData.hIcon && DestroyIcon(niData.hIcon))
		niData.hIcon = NULL;

	// call ShowWindow here to make the dialog initially visible

	return TRUE;
}

BOOL OnInitDialog(HWND hWnd)
{
	HMENU hMenu = GetSystemMenu(hWnd,FALSE);
	if (hMenu)
	{
		AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
		AppendMenu(hMenu, MF_STRING, IDM_ABOUT, _T("About"));
	}
	HICON hIcon = (HICON)LoadImage(hInst,
		MAKEINTRESOURCE(IDI_STEALTHDLG),
		IMAGE_ICON, 0,0, LR_SHARED|LR_DEFAULTSIZE);
	SendMessage(hWnd,WM_SETICON,ICON_BIG,(LPARAM)hIcon);
	SendMessage(hWnd,WM_SETICON,ICON_SMALL,(LPARAM)hIcon);
	return TRUE;
}

// Name says it all
void ShowContextMenu(HWND hWnd)
{
	POINT pt;
	GetCursorPos(&pt);
	HMENU hMenu = CreatePopupMenu();
	if(hMenu)
	{
		if( IsWindowVisible(hWnd) )
			InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_HIDE, _T("Hide"));
		else
			InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_SHOW, _T("Show"));
		InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_EXIT, _T("Exit"));

		// note:	must set window to the foreground or the
		//			menu won't disappear when it should
		SetForegroundWindow(hWnd);

		TrackPopupMenu(hMenu, TPM_BOTTOMALIGN,
			pt.x, pt.y, 0, hWnd, NULL );
		DestroyMenu(hMenu);
	}
}

// Get dll version number
ULONGLONG GetDllVersion(LPCTSTR lpszDllName)
{
    ULONGLONG ullVersion = 0;
	HINSTANCE hinstDll;
    hinstDll = LoadLibrary(lpszDllName);
    if(hinstDll)
    {
        DLLGETVERSIONPROC pDllGetVersion;
        pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, "DllGetVersion");
        if(pDllGetVersion)
        {
            DLLVERSIONINFO dvi;
            HRESULT hr;
            ZeroMemory(&dvi, sizeof(dvi));
            dvi.cbSize = sizeof(dvi);
            hr = (*pDllGetVersion)(&dvi);
            if(SUCCEEDED(hr))
				ullVersion = MAKEDLLVERULL(dvi.dwMajorVersion, dvi.dwMinorVersion,0,0);
        }
        FreeLibrary(hinstDll);
    }
    return ullVersion;
}

// Message handler for the app
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	static TCHAR dlg_string[1024];

	switch (message) 
	{
	case SWM_TRAYMSG:
		switch(lParam)
		{
		case WM_LBUTTONDBLCLK:
			ShowWindow(hWnd, SW_RESTORE);
			break;
		case WM_RBUTTONDOWN:
		case WM_CONTEXTMENU:
			ShowContextMenu(hWnd);
		}
		break;
	case WM_SYSCOMMAND:
		if((wParam & 0xFFF0) == SC_MINIMIZE)
		{
			ShowWindow(hWnd, SW_HIDE);
			return 1;
		}
		else if(wParam == IDM_ABOUT)
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam); 

		switch (wmId)
		{
		case SWM_SHOW:
			ShowWindow(hWnd, SW_RESTORE);
			break;
		case SWM_HIDE:
		case IDOK:
			if ( BST_CHECKED == IsDlgButtonChecked(hWnd,IDC_USE_REMOTE_ADDR)){
				saved_options.ipaddr_type = USE_REMOTE_ADDR;
			}else{
				saved_options.ipaddr_type = SEND_CUSTOM_IP;
			}
			
			GetDlgItemText(hWnd,IDC_IPADDRIP,dlg_string,IDC_USE_CUSTOM_IP);
			
			int i;

			for (i=0;i<16;i++)
			{
				saved_options.custom_ip_addr[i]=(char)dlg_string[i];
			}

			GetDlgItemText(hWnd,IDC_EDIT_DOMAIN_NAME,dlg_string,IDC_EDIT_DOMAIN_NAME);

			for (i=0;i<1024;i++)
			{
				if (dlg_string[i]==0)
				{
					saved_options.domain_name[i]=0;
					break;
				}
				saved_options.domain_name[i]=(char)dlg_string[i];
			}

			GetDlgItemText(hWnd,IDC_EDIT_LOGIN,dlg_string,IDC_EDIT_LOGIN);

			for (i=0;i<1024;i++)
			{
				if (dlg_string[i]==0)
				{
					saved_options.login[i]=0;
					break;
				}
				saved_options.login[i]=(char)dlg_string[i];
			}


			GetDlgItemText(hWnd,IDC_EDIT_PASS,dlg_string,IDC_EDIT_PASS);

			for (i=0;i<1024;i++)
			{
				if (dlg_string[i]==0)
				{
					saved_options.password[i]=0;
					break;
				}
				saved_options.password[i]=(char)dlg_string[i];
			}


			GetDlgItemText(hWnd,IDC_EDIT_URL,dlg_string,IDC_EDIT_URL);

			for (i=0;i<1024;i++)
			{
				if (dlg_string[i]==0)
				{
					saved_options.dtc_panel_url[i]=0;
					break;
				}
				saved_options.dtc_panel_url[i]=(char)dlg_string[i];
			}

			saved_options.update_interval=-1;

			if (BST_CHECKED == IsDlgButtonChecked(hWnd,IDC_RADIO_10MIN))
			{
				saved_options.update_interval=0;
			}
			if (BST_CHECKED == IsDlgButtonChecked(hWnd,IDC_RADIO_30MIN))
			{
				saved_options.update_interval=1;
			}
			if (BST_CHECKED == IsDlgButtonChecked(hWnd,IDC_RADIO_60MIN))
			{
				saved_options.update_interval=2;
			} 	
     		if ( BST_CHECKED == IsDlgButtonChecked(hWnd,IDC_RADIO_3600MIN))
			{
				saved_options.update_interval=3;
			} 

			FILE* fp;
			fp = fopen("dtc_dynip_pref.dat","wb+");
			fwrite(&saved_options,1,sizeof(saved_options),fp);
			fclose(fp);

			switch (saved_options.update_interval) {
				case 0:
					SetTimer (hWnd, 1, 1000 * 60 * 10, NULL) ;
					break;
				case 1:
					SetTimer (hWnd, 1, 1000 * 60 * 30, NULL) ;
					break;
				case 2:
					SetTimer (hWnd, 1, 1000 * 60 * 60, NULL) ;
					break;
				case 3:
					SetTimer (hWnd, 1, 1000 * 60 * 1400, NULL) ;
					break;
			}
			ShowWindow(hWnd, SW_HIDE);
			break;
		case SWM_EXIT:
			DestroyWindow(hWnd);
			break;
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
		}
		return 1;
	case WM_INITDIALOG:
		// Complete dialog fields
		if (saved_options.ipaddr_type == USE_REMOTE_ADDR)
			CheckDlgButton(hWnd, IDC_USE_REMOTE_ADDR, BST_CHECKED);
		else {
			CheckDlgButton(hWnd, IDC_USE_CUSTOM_IP, BST_CHECKED);
			SetDlgItemText(hWnd, IDC_IPADDRIP, saved_options.custom_ip_addr);				
		}
		SetDlgItemText(hWnd, IDC_EDIT_DOMAIN_NAME, saved_options.domain_name);	
		SetDlgItemText(hWnd, IDC_EDIT_LOGIN, saved_options.login);
		SetDlgItemText(hWnd, IDC_EDIT_PASS, saved_options.password);
		SetDlgItemText(hWnd, IDC_EDIT_URL, saved_options.dtc_panel_url);

		switch (saved_options.update_interval) {
			case 0:
				CheckDlgButton(hWnd, IDC_RADIO_10MIN, BST_CHECKED);
				break;
			case 1:
				CheckDlgButton(hWnd, IDC_RADIO_30MIN, BST_CHECKED);
				break;
			case 2:
				CheckDlgButton(hWnd, IDC_RADIO_60MIN, BST_CHECKED);
				break;
			case 3:
				CheckDlgButton(hWnd, IDC_RADIO_3600MIN, BST_CHECKED);
				break;
		}


		return OnInitDialog(hWnd);
	case WM_TIMER:
		switch (wParam) {
			case 1:
				char* query;
				// obtain the host from the url: ex: http://www.example.com/dtc/script.php -> www.example.com
				char* host = get_hostname(saved_options.dtc_panel_url, 256);
				BIO		*conn = NULL;
				SSL		*ssl = NULL;
				SSL_CTX	*ctx = NULL;
				struct addrinfo hints, *res;
				FILE *fp;

				WSAData data;
				WSAStartup(MAKEWORD(2,0), &data);

				init_OpenSSL();
				//seed_prng();
				

				memset(&hints, 0, sizeof(hints));
				hints.ai_socktype = SOCK_STREAM;
				hints.ai_family = AF_INET;
				if (getaddrinfo(host, NULL, &hints, &res) != 0) {
						//MessageBox(NULL, L"Invalid host name", L"ERROR", NULL);
						fp = fopen(LOG_FILE, "a+");
						char* url = unicode2ascii(saved_options.dtc_panel_url);
						fprintf(fp, "Can not resolve DTC url: %s @ %s\n", url, get_current_time());
						fclose(fp);
						free(url);
						// changing the tray icon
						Shell_NotifyIcon(NIM_DELETE,&niData);
						niData.hIcon = (HICON)LoadImage(hInst,MAKEINTRESOURCE(IDI_STEALTHDLG_ERROR),
		IMAGE_ICON, GetSystemMetrics(SM_CXSMICON),GetSystemMetrics(SM_CYSMICON),
		LR_DEFAULTCOLOR);
						Shell_NotifyIcon(NIM_ADD,&niData);
						break;
				 }



				// compute the request
				query = computeRequest(&saved_options, host);

				// make connection
				ssl = doSSLConnection(host, ctx);
				if (ssl == NULL) {
					fp = fopen(LOG_FILE, "a+");
					fprintf(fp, "Error initiating HTTP/SSL connection to server: %s@%s\n", host, get_current_time());
					fclose(fp);
					conn = doHTTPConnection(host);
					if (conn == NULL) {
						//MessageBox(NULL, L"HTTP Connection failed.\nLogging event", L"ERROR", NULL);
						fp = fopen(LOG_FILE, "a+");
						fprintf(fp, "Error initiating HTTP connection to server: %s@%s\n", host, get_current_time());
						fclose(fp);
						// changing the tray icon
						Shell_NotifyIcon(NIM_DELETE,&niData);
						niData.hIcon = (HICON)LoadImage(hInst,MAKEINTRESOURCE(IDI_STEALTHDLG_ERROR),
		IMAGE_ICON, GetSystemMetrics(SM_CXSMICON),GetSystemMetrics(SM_CYSMICON),
		LR_DEFAULTCOLOR);
						Shell_NotifyIcon(NIM_ADD,&niData);
						break;
					}
				}

				//debugPrint(query);

				if (ssl == NULL) {
					sendHTTPRequest(conn, query, 1000);
					receiveHTTPResponse(conn, query, 1000);
				} else {
					sendSSLRequest(ssl, query, 1000); 
					receiveSSLResponse(ssl, query, 1000);						
				}
				
				if (ssl != NULL || conn != NULL) {
					// changing the tray icon
					Shell_NotifyIcon(NIM_DELETE,&niData);
					niData.hIcon = (HICON)LoadImage(hInst,MAKEINTRESOURCE(IDI_STEALTHDLG),
													IMAGE_ICON, GetSystemMetrics(SM_CXSMICON),
													GetSystemMetrics(SM_CYSMICON),
													LR_DEFAULTCOLOR);
					Shell_NotifyIcon(NIM_ADD,&niData);
				}

				// shutdown the connection
				if (ssl) {
					SSL_shutdown(ssl);
					SSL_free(ssl);
					SSL_CTX_free(ctx);
				}
				if (conn) {
					BIO_free(conn);
				}
				
				// show what has been received for debugging				
				//debugPrint(query);

				//free any allocated space
				free(query);
				free(host);
				break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		KillTimer(hWnd, 1);
		break;
	case WM_DESTROY:
		niData.uFlags = 0;
		Shell_NotifyIcon(NIM_DELETE,&niData);
		PostQuitMessage(0);
		break;
	}
	return 0;
}


typedef struct {
	char* ip_string;
}t_dtcwindns_cfg;

t_dtcwindns_cfg dtcwindns_cfg;

// Message handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
	return FALSE;
}
