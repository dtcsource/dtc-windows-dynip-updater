/****************************************************************************
 * Copyright 2009 Dan Bizdadea (dan.bizdadea@gmail.com)
 ****************************************************************************/

/****************************************************************************
 *	This file is part of Dynamic IP Update Client.
 *
 *  Dynamic IP Update Client is free software: you can redistribute it and/or 
 *	modify it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dynamic IP Update Client is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dynamic IP Update Client.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Win32 Dialog.rc
//
#define IDR_MANIFEST                    1
#define IDD_DLG_DIALOG                  102
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDC_MYICON                      105
#define IDC_STEALTHDIALOG               106
#define IDI_STEALTHDLG                  107
#define IDI_STEALTHDLG_ERROR			108
#define IDC_IPADDRIP                    1000
#define IDC_USE_CUSTOM_IP               1001
#define IDC_USE_REMOTE_ADDR             1002
#define IDC_EDIT_DOMAIN_NAME            1003
#define IDC_EDIT_LOGIN                  1004
#define IDC_EDIT_PASS                   1005
#define IDC_RADIO_10MIN                 1006
#define IDC_RADIO_30MIN                 1007
#define IDC_RADIO_60MIN                 1008
#define IDC_EDIT_URL                    1009
#define IDC_RADIO6                      1010
#define IDC_RADIO_3600MIN               1010
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
