/****************************************************************************
 * Copyright 2009 Dan Bizdadea (dan.bizdadea@gmail.com)
 ****************************************************************************/

/****************************************************************************
 *	This file is part of Dynamic IP Update Client.
 *
 *  Dynamic IP Update Client is free software: you can redistribute it and/or 
 *	modify it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dynamic IP Update Client is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dynamic IP Update Client.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

/* INCLUDES SECTION */
#include "stdafx.h"

/* DEFINES SECTION */
#define LOG_FILE	"error.log"

/* FUNCTIONS SECTION */
char* get_hostname(TCHAR* url, int size);
char* computeRequest(t_so* so, char* host);
void init_OpenSSL(void);
int sendHTTPRequest(BIO* conn, char* request, int len);
int receiveHTTPResponse(BIO* conn, char* response, int len);
int sendSSLRequest(SSL* ssl, char* request, int len);
int receiveSSLResponse(SSL* ssl, char* response, int len);
SSL* doSSLConnection(char* host, SSL_CTX* ctx);
BIO* doHTTPConnection(char* host);
void debugPrint(char* buffer);
void handle_error(const char *msg);
char* get_current_time();
char* unicode2ascii(TCHAR* unistring);
