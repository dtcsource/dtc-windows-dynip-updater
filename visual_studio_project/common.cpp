/****************************************************************************
 * Copyright 2009 Dan Bizdadea (dan.bizdadea@gmail.com)
 ****************************************************************************/

/****************************************************************************
 *	This file is part of Dynamic IP Update Client.
 *
 *  Dynamic IP Update Client is free software: you can redistribute it and/or 
 *	modify it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dynamic IP Update Client is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dynamic IP Update Client.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "stdafx.h"
#include "common.h"

void handle_error(const char *msg)
{
	FILE *fp;
	fp = fopen(LOG_FILE, "w");
	fprintf(fp, "** %s\n", msg);
	ERR_print_errors_fp(fp);
	fclose(fp);
}


void init_OpenSSL(void)
{
	if (! SSL_library_init())
	{
		MessageBox(NULL, L"OpenSSL initialization failed!", L"Fatal Error", NULL);
		exit(-1);
	}
	SSL_load_error_strings();
}


/* http://www.domain.com/path/to/script --> www.domain.com */
char* get_hostname(TCHAR* url, int size) {
	int start = 0;
	char *host;

	host = (char*) malloc (size * sizeof(char));

	while (start < size) {
		if (url[start] == '/')
			break;
		start++;
	}
	start += 2; // get the // after http:
	for (int i=start; i<size; i++) {
		if (url[i] != '/')
			host[i-start] = (char)url[i];
		else {
			host[i-start] = 0;
			break;
		}
	}
	return host;
}

/* :) */
char* get_current_time() {
	time_t now;
	time(&now);
	return ctime(&now);
}

/* convert a unicode text to ascii */
char* unicode2ascii(TCHAR* uni_text) {
	unsigned int i;
	char* ascii_text = (char*) malloc((wcslen(uni_text) + 1) * sizeof(char));
	ZeroMemory(ascii_text, wcslen(uni_text)+1);
	for (i=0; i < wcslen(uni_text); i++) {
		ascii_text[i] = (char)uni_text[i];
	}

	return ascii_text;
}


/* builds the request to be sent to the server */
char* computeRequest(t_so* so, char* host) {
	char* a_url = unicode2ascii(so->dtc_panel_url);
	char* a_domain = unicode2ascii(so->domain_name);
	char* a_login = unicode2ascii(so->login);
	char* a_pass = unicode2ascii(so->password);
	char* a_ip;
	
	if (so->ipaddr_type == SEND_CUSTOM_IP)
		a_ip = unicode2ascii(so->custom_ip_addr);
	else a_ip = "";

	char* query = (char *) malloc(1000 * sizeof(char));

	ZeroMemory(query, 1000);
	sprintf(query, "GET /dtc/dynip.php?domain=%s&login=%s&pass=%s", a_domain, a_login, a_pass);
	if (a_ip != "") {
		strcat(query, "&ip=");
		strcat(query, a_ip);
	}
	strcat(query, " HTTP/1.0\r\nHost: ");
	strcat(query, host);
	strcat(query, "\r\nConnection: close\r\n\r\n");
	free(a_url); free(a_domain); free(a_login); free(a_pass); free(a_ip);

	return query;
}
/*	sends the request over the http connection.
	Params: BIO* conn: the http connection socket
			char* request: the request we send
	Return value: 
		0 - an error has occured
		1 - OK
	Created: 14/03/2009
	Author: Dan Bizdadea (dan.bizdadea@gmail.com)
*/
int sendHTTPRequest(BIO* conn, char* request, int len) {
	int err;
	int retcode = 1;

	for (int nwritten = 0; nwritten < len; nwritten += err)
	{
		err = BIO_write(conn, request + nwritten, (int)strlen(request) - nwritten);
		if (err <= 0) {
			if (nwritten < (int)strlen(request)) retcode = 0;
			break;
		}
	}
	return retcode;
}

/*	receive the response over the http connection.
	Params: BIO* conn: the http connection socket
			char* response: the request we send
			int len: maximum length of buffer
	Return value: 
		0 - an error has occured
		1 - OK
	Created: 14/03/2009
	Author: Dan Bizdadea (dan.bizdadea@gmail.com)
*/
int receiveHTTPResponse(BIO* conn, char* response, int len) {
	int err;
	int retcode = 1;

	ZeroMemory(response, len);
	for (int nread = 0; nread < len; nread += err)
	{
		err = BIO_read(conn, response + nread, len - nread);
		if (err <= 0) {
			if (nread == 0) retcode = 0;
			break;
		}
	}

	return retcode;
}



/*	sends the request over the https connection.
	Params: SSL* ssl: the ssl connection socket
			char* request: the request we send
	Return value: 
		0 - an error has occured
		1 - OK
	Created: 13/03/2009
	Author: Dan Bizdadea (dan.bizdadea@gmail.com)
*/
int sendSSLRequest(SSL* ssl, char* request, int len) {
	int err;
	int retcode = 1;

	for (int nwritten = 0; nwritten < len; nwritten += err)
	{
		err = SSL_write(ssl, request + nwritten, (int)strlen(request) - nwritten);
		if (err <= 0) {
			if (nwritten < (int)strlen(request)) retcode = 0;
			break;
		}
	}
	return retcode;
}

/*	receive the response over the https connection.
	Params: SSL* ssl: the ssl connection socket
			char* response: the request we send
			int len: maximum length of buffer
	Return value: 
		0 - an error has occured
		1 - OK
	Created: 14/03/2009
	Author: Dan Bizdadea (dan.bizdadea@gmail.com)
*/
int receiveSSLResponse(SSL* ssl, char* response, int len) {
	int err;
	int retcode = 1;

	ZeroMemory(response, len);
	for (int nread = 0; nread < len; nread += err)
	{
		err = SSL_read(ssl, response + nread, len - nread);
		if (err <= 0) {
			if (nread == 0) retcode = 0;
			break;
		}
	}

	return retcode;
}

/* prints an ascii text using MessageBox */
void debugPrint(char* buffer) {
	wchar_t msg[1000];
	
	ZeroMemory(msg, 1000);
	for (int i=0; i<1000; i++)
		msg[i] = (wchar_t)buffer[i];

	MessageBox(NULL, msg, L"Title here", NULL); 
}


/* initiates a connection with HTTP */
BIO* doHTTPConnection(char* host) {
	char host_port[512];
	BIO* conn;

	sprintf(host_port, "%s:%d", host, 80);
	conn = BIO_new_connect(host_port);
	if (!conn) {
		return NULL;
	}

	if (BIO_do_connect(conn) <= 0) {		
		return NULL;
	}	

	return conn;
}


/* initiates a connection with HTTP over SSL */
SSL* doSSLConnection(char* host, SSL_CTX* ctx) {
	ctx = SSL_CTX_new(SSLv23_method());
	char host_port[512];
	BIO* conn;
	SSL* ssl;

	sprintf(host_port, "%s:%d", host, 443);

	conn = BIO_new_connect(host_port);		
	if (!conn) {
		return NULL;
	}

	if (BIO_do_connect(conn) <= 0) {
		return NULL;
	}	

	if (!(ssl = SSL_new(ctx))) {
		return NULL;
	}
	
	SSL_set_bio(ssl, conn, conn);
	if (SSL_connect(ssl) <= 0) {
		//MessageBox(NULL, L"Error connecting SSL object", L"OpenSSL Error", NULL);
		return NULL;
	}

	return ssl;
}
