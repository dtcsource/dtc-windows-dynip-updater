/****************************************************************************
 * Copyright 2009 Thomas Goirand
 * 
 ****************************************************************************/

/****************************************************************************
 *	This file is part of Dynamic IP Update Client.
 *
 *  Dynamic IP Update Client is free software: you can redistribute it and/or 
 *	modify it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dynamic IP Update Client is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dynamic IP Update Client.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

// Saved options typedef

typedef enum {
	USE_REMOTE_ADDR=1,
	SEND_CUSTOM_IP=2
} t_so_ipaddr_type;

typedef struct{
	t_so_ipaddr_type ipaddr_type;
	TCHAR custom_ip_addr[16];
	TCHAR  domain_name[256];
	TCHAR  login[64];
	TCHAR  password[64];
	TCHAR  dtc_panel_url[256];
	int update_interval;	// number of seconds between each IP update
}t_so;
